package com.task.spring.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController

public class AccountController {

	@RequestMapping(value = "users/superAdmin")
	public String SuperAdmin() {
		return ("Hello and welcome super Admin");
	}

	@RequestMapping(value = "users/admin")
	public String Admin() {
		return ("Hello and welcome Admin");
	}

	@RequestMapping(value = "users/user")
	public String User() {
		return ("Hello and welcome user");
	}

	@RequestMapping(value = "users/staff")
	public String Staff() {
		return ("Hello and welcome staff");
	}

}
