package com.task.spring.configure;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	@Override
	public void configure(HttpSecurity httpSecurity) throws Exception {
		httpSecurity.authorizeRequests().antMatchers("/users/superAdmin").hasRole("SUPER_ADMIN")
				.antMatchers("/users/admin").hasRole("ADMIN").antMatchers("/users/user").hasRole("USER")
				.antMatchers("/users/staff").hasRole("STAFF").and().formLogin();

	}

	@Autowired
	public void configureGlobalSecurity(AuthenticationManagerBuilder auth) throws Exception {
		auth.inMemoryAuthentication().withUser("superAdmin").password("{noop}superadmin").roles("SUPER_ADMIN", "ADMIN",
				"USER", "STAFF");
		auth.inMemoryAuthentication().withUser("admin").password("{noop}admin").roles("ADMIN", "USER", "STAFF");
		auth.inMemoryAuthentication().withUser("user").password("{noop}user").roles("USER");
		auth.inMemoryAuthentication().withUser("staff").password("{noop}staff").roles("STAFF");
	}

}
